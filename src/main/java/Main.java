import java.sql.*;
import java.util.List;

public class Main {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:5432/company";

    //  Database credentials
    static final String USER = "postgres";
    static final String PASS = "postgres";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            conn.setAutoCommit(false);

            System.out.println("Creating statement...");
            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            String SQL = "";
            SQL = "CREATE TABLE IF NOT EXISTS Employees(id INTEGER PRIMARY KEY, age INT, first TEXT, last TEXT)";
            stmt.execute(SQL);
            stmt.execute("TRUNCATE Employees");
            SQL = "CREATE TABLE IF NOT EXISTS salary(id INTEGER PRIMARY KEY, employee_id INTEGER UNIQUE, amount INTEGER)";
            stmt.execute(SQL);
            stmt.execute("TRUNCATE Salary");
            conn.commit();

            List<Integer> ids = List.of(1, 2, 3);
            List<Integer> employees = List.of(1, 2, 3);
            List<Integer> amounts = List.of(1, 2, 3);
            addSalaries(ids, employees, amounts);

            System.out.println("Inserting one row....");
            SQL = "INSERT INTO Employees " +
                    "VALUES (108, 20, 'Jane', 'Eyre')";

            stmt.executeUpdate(SQL);
            // Commit data here.
            System.out.println("Commiting data here....");
            conn.commit();

            System.out.println("Inserting one row....");
            SQL = "INSERT INTO Employees " +
                    "VALUES (109, 20, 'David', 'Rochester')";

            stmt.executeUpdate(SQL);


            System.out.println("Inserting one row....");
            SQL = "INSERT INTO Employees " +
                    "VALUES (108, 20, 'Rita', 'Tez')";


            System.out.println("Inserting one row....");
            SQL = "INSERT INTO Employees " +
                    "VALUES (106, 20, 'Rita', 'Tez')";
            stmt.executeUpdate(SQL);
            SQL = "INSERT INTO Employees " +
                    "VALUES (107, 22, 'Sita', 'Singh')";
            stmt.executeUpdate(SQL);

            // Commit data here.
            System.out.println("Commiting data here....");
            conn.commit();

            String sql = "SELECT id, first, last, age FROM Employees";
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("List result set for reference....");
            printRs(rs);
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }//end try

        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }
        System.out.println("Goodbye!");
    }

    public static void printRs(ResultSet rs) throws SQLException {
        //Ensure we start with first row
        rs.beforeFirst();
        while (rs.next()) {
            //Retrieve by column name
            int id = rs.getInt("id");
            int age = rs.getInt("age");
            String first = rs.getString("first");
            String last = rs.getString("last");

            //Display values
            System.out.print("ID: " + id);
            System.out.print(", Age: " + age);
            System.out.print(", First: " + first);
            System.out.println(", Last: " + last);
        }
        System.out.println();
    }


    public static void addSalaries(List<Integer> ids, List<Integer> employee_ids, List<Integer> amount) {
        assert ids.size() == employee_ids.size() && ids.size() == amount.size();
        Connection con = null;
        Statement stmt = null;
        try {
            con = DriverManager.getConnection(DB_URL, USER, PASS);
            con.setAutoCommit(false);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

            for (int i = 0; i < ids.size(); i++) {
                String sql = String.format("INSERT INTO Salary VALUES (%d, %d, %d)", ids.get(i), employee_ids.get(i), amount.get(i));
                stmt.executeUpdate(sql);
                con.commit();
            }
        } catch (SQLException e) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
